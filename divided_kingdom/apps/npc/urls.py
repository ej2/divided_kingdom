from django.conf.urls import patterns, url
from divided_kingdom.apps.core.patterns import ID
from divided_kingdom.apps.npc import views


urlpatterns = patterns(
    "",
    url(r"^/random$", views.create_random, name="random"),
    url(r"^/(?P<id>%s)$" % ID, views.detail, name="detail"),
)