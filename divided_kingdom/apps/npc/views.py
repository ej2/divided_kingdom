from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, redirect
from django.template import RequestContext

from django.template.response import TemplateResponse
from divided_kingdom.apps.npc.helpers.generator import generate_basic_npc

from divided_kingdom.apps.npc.models import NPC


@login_required
def detail(request, id):
    npc = get_object_or_404(NPC, pk=id)

    context = RequestContext(request, {
        "npc": npc,
    })

    return TemplateResponse(request, "npc/detail.html", context)


@login_required
def create_random(request):
    npc = generate_basic_npc()

    return redirect("npc:detail", npc.pk)
