import os
import random
from titlecase import titlecase
from divided_kingdom import settings
from divided_kingdom.apps.core.utils import random_line
from divided_kingdom.apps.npc.models import NPC


def generate_basic_npc():
    gender = random.choice(["M", "F"])

    if gender == "F":
        file_name = get_file_path("data/female_names.txt")
    else:
        file_name = get_file_path("data/male_names.txt")

    first_name = format_name(random_line(file_name))
    last_name = format_name(random_line(get_file_path("data/last_names.txt")))

    npc = NPC.objects.create(
        name="{0} {1}".format(first_name, last_name),
        age=random.randint(10, 80),
        gender=gender
    )

    return npc


def format_name(name):
    return titlecase(name.strip())


def get_file_path(file_name):
    return os.path.join(settings.STATIC_ROOT, file_name)
