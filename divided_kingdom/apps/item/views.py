from annoying.functions import get_object_or_None
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.template.response import TemplateResponse

from divided_kingdom.apps.item.helpers.item import get_random_drop, add_item_type_to_player
from divided_kingdom.apps.item.models import DropTable
from divided_kingdom.apps.player.models import Player


@login_required
def create_random(request):
    user = request.user
    player = get_object_or_None(Player, user=user)

    drop_table = DropTable.objects.get(pk=1)
    item_type = get_random_drop(drop_table)
    item = add_item_type_to_player(item_type, player)

    context = RequestContext(request, {
        "user": user,
        "item": item
    })

    return TemplateResponse(request, "item/detail.html", context)
