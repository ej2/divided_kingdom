
from django import template
from django.template.loader import render_to_string

register = template.Library()


@register.simple_tag()
def display_equipment(label, equipment):

    return render_to_string("item/equipment_display.html", {
        "label": label,
        "equipment": equipment
        })
