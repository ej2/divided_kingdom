

def equip(item, target):

    if item.classification == "HEAD":
        target.head = item
    elif item.classification == "CHEST":
        target.chest = item
    elif item.classification == "GLOVE":
        target.gloves = item
    elif item.classification == "BOOTS":
        target.feet = item
    elif item.classification == "LEGS":
        target.legs = item
    elif item.classification == "MOUNT":
        target.mount = item
    elif item.classification == "JEWLRY":
        if target.right_hand is None:
            target.right_hand = item
        else:
            target.left_hand = item

    target.save()

def remove(item, target):
    if item.classification == "HEAD":
        target.head = None
    elif item.classification == "CHEST":
        target.chest = None
    elif item.classification == "GLOVE":
        target.gloves = None
    elif item.classification == "BOOTS":
        target.feet = None
    elif item.classification == "LEGS":
        target.legs = None
    elif item.classification == "MOUNT":
        target.mount = None
    elif item.classification == "JEWLRY":
        if target.right_hand == item:
            target.right_hand = None
        elif target.left_hand == item:
            target.left_hand = None

    target.save()
