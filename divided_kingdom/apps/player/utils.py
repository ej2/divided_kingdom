from annoying.functions import get_object_or_None
from divided_kingdom.apps.core.game_settings import STARTING_LOCATION_ID
from divided_kingdom.apps.location.models import Location


def new_player_setup(player):
    player.location = get_object_or_None(Location, pk=STARTING_LOCATION_ID)
    player.status = "L"
    player.save()