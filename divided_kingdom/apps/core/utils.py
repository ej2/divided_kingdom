import random

# Returns random line from a file
def random_line(afile):
    return random.choice(open(afile, 'r').readlines())

