import random
from divided_kingdom.apps.core.game_settings import BASE_ESCAPE_CHANCE, RUN_STAMINA_COST, BASE_MOB_ESCAPE_CHANCE, \
    CRITICAL, CRITICAL_MULTIPLER, FUMBLE, STRENGTH_DAMAGE_BONUS


COMBAT_SWING_MESSAGE = (
    ("You strike at the {0} and ", ),
    ("You carefully slash at the {0} and ", ),
    ("You thrust your weapon at the {0} and ", ),
)

COMBAT_HIT_MESSAGE = (
    ("you hit",),
)

COMBAT_MISS_MESSAGE = (
    ("your attack misses.", ),
    ("your target leaps out of the way of your attack.", ),
    ("you clumsly miss your target",)
)

class AttackOutcome():
    fumble = 1
    miss = 2
    hit = 3
    critical = 4


def attack_outcome(attacker, defender):
    attack_roll = random.randint(1, 20)
    attack_score = attacker.attack + attack_roll

    if attack_roll <= FUMBLE:
        return AttackOutcome.fumble
    elif attack_roll >= CRITICAL:
        return AttackOutcome.critical
    elif attack_score > defender.defense:
        return AttackOutcome.hit
    else:
        return AttackOutcome.miss


def calculate_damage(attacker, min_damage, max_damage, critical):
    damage_amount = random.randint(min_damage, max_damage)
    damage_bonus = round(attacker.strength * STRENGTH_DAMAGE_BONUS)

    if critical:
        damage_amount = damage_amount * CRITICAL_MULTIPLER

    return int(damage_amount + damage_bonus)


def escape_combat(player, mob):
    escape_chance = BASE_ESCAPE_CHANCE + player.speed - mob.speed

    if random.randint(1, 100) < escape_chance:
        return True
    else:
        return False


def end_combat(player, mob):
    player.status = "D"
    player.save()

    mob.active = False
    mob.save()