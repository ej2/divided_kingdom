from django.conf.urls import patterns, url
from divided_kingdom.apps.game.views import hack


urlpatterns = patterns(
    "",
    url(r"^/reset$", hack.player_reset, name="reset"),
    url(r"^/quicktravel", hack.quick_travel, name="quicktravel"),
)