from annoying.functions import get_object_or_None
from django.shortcuts import redirect
from divided_kingdom.apps.player.models import Player

# Remove - for testing only

def quick_travel(request):
    user = request.user
    player = get_object_or_None(Player, user=user)

    player.distance_marker = 0
    player.location = player.route.end_location
    player.route = None
    player.status = "L"

    player.save()

    return redirect("game:play")


# Remove - for testing only
def player_reset(request):
    user = request.user
    player = get_object_or_None(Player, user=user)

    player.current_health = player.total_health
    player.current_stamina = player.total_stamina

    player.save()

    return redirect("game:play")
