import random

from annoying.functions import get_object_or_None
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.template import RequestContext
from django.template.response import TemplateResponse

from divided_kingdom.apps.core.game_settings import RUN_STAMINA_COST, ATTACK_STAMINA_COST
from divided_kingdom.apps.game.helpers.combat import escape_combat, end_combat, \
    attack_outcome, AttackOutcome, calculate_damage
from divided_kingdom.apps.game.helpers.event import resolve_event, generate_event
from divided_kingdom.apps.game.helpers.helper import get_game_messages, create_game_message
from divided_kingdom.apps.game.helpers.reward import calculate_reward, combat_reward
from divided_kingdom.apps.game.models import EventAction, EventLog
from divided_kingdom.apps.location.models import Route
from divided_kingdom.apps.mob.helpers.encounter import random_encounter
from divided_kingdom.apps.mob.models import Mob
from divided_kingdom.apps.player.models import Player


@login_required
def play(request):
    user = request.user
    player = get_object_or_None(Player, user=user)

    if player is None:
        return redirect("player:new")

    context = {
        "user": player.user,
        "player": player,
    }

    if player.status == 'C': #Combat
        mob = get_object_or_None(Mob, player=player, active=True)

        template = "game/combat.html"
        context["mob"] = mob

    elif player.status == 'E': #Event
        event_log = get_object_or_None(EventLog, player=player, resolved=False)
        #Player is in an Event
        template = "game/event.html"
        context["event"] = event_log.game_event

    elif player.status == 'L': #Location
        template = "game/location.html"
        context["location"] = player.location

    elif player.status == 'R': #Route
        #Player is traveling on route
        template = "game/route.html"
        context["route"] = player.route
        context["remaining_miles"] = player.route.distance - player.distance_marker

        event_chance = random.randint(1, 100)

        if event_chance < player.route.event_chance:
            combat_chance = random.randint(1, 100)

            if combat_chance < player.route.combat_chance:
                mob = random_encounter(player)
                player.status = 'C'
                player.save()

                template = "game/combat.html"
                context["mob"] = mob

            else:
                event = generate_event(player)
                player.status = 'E'
                player.save()

                template = "game/event.html"
                context["event"] = event

    elif player.status == 'D': #Event/Combat End
        context["route"] = player.route
        context["remaining_miles"] = player.route.distance - player.distance_marker

        player.status = 'R'
        player.save()

        template = "game/event_end.html"

    context["game_messages"] = get_game_messages(player)
    return TemplateResponse(request, template, RequestContext(request, context))


def forward(request):
    user = request.user
    player = get_object_or_None(Player, user=user)
    player.status = 'R'
    player.distance_marker += 1

    if player.distance_marker >= player.route.distance:
        player.distance_marker = 0
        player.location = player.route.end_location
        player.route = None
        player.status = "L"

    player.save()
    return redirect("game:play")


def backward(request):
    user = request.user
    player = get_object_or_None(Player, user=user)
    player.status = 'R'
    player.distance_marker -= 1

    if player.distance_marker <= 0:
        player.distance_marker = 0
        player.location = player.route.start_location
        player.route = None
        player.status = "L"

    player.save()
    return redirect("game:play")


def travel(request, route_id):
    user = request.user
    player = get_object_or_None(Player, user=user)

    player.route = get_object_or_None(Route, pk=route_id)
    player.distance_marker = 0
    player.status = 'R'
    player.save()

    return redirect("game:play")


def action(request, action_id):
    user = request.user
    player = get_object_or_None(Player, user=user)

    event_action = get_object_or_None(EventAction, id=action_id)

    i = random.randint(1, 100)

    if i <= event_action.success_chance:
        #SUCCESS!
        reward_message = calculate_reward(player, event_action, True)

        create_game_message(player, "{0} {1}{2}".format(
            event_action.action_text, event_action.success_text, reward_message))

    else:
        #FAILURE!
        reward_message = calculate_reward(player, event_action, False)
        create_game_message(player, "{0} {1}{2}".format(
            event_action.action_text, event_action.failure_text, reward_message))

    resolve_event(player, event_action.event)
    return redirect("game:play")


def search(request):
    user = request.user
    player = get_object_or_None(Player, user=user)

    random_event = random.randint(1, 8)

    if random_event < 6:
        result = "You look around and find nothing of interest."

    elif random_event == 6:
        player.distance_marker += 2
        player.save()

        result = "You find a shortcut through the woods that gets you a little closer to {0}.".format(
            player.route.end_location.name)

    elif random_event == 7:
        player.distance_marker -= 2
        player.save()

        result = "You get lost in the woods and find yourself a little further from {0}.".format(
            player.route.end_location.name)

    elif random_event == 8:
        coins = random.randint(2, 8)
        player.gold += coins
        player.save()

        location = random.randint(1, 5)

        if location == 1:
            result = "You find {0} gold coins in the bushes.".format(coins)
        elif location == 2:
            result = "You find {0} gold coins in the dust.".format(coins)
        elif location == 3:
            result = "You find {0} gold coins in the tall grass.".format(coins)
        elif location == 4:
            result = "You find {0} gold coins under a rock.".format(coins)
        elif location == 5:
            result = "You find {0} gold coins in the weeds.".format(coins)

    create_game_message(player, result)
    return redirect("game:play")


def rest(request):
    user = request.user
    player = get_object_or_None(Player, user=user)

    stamina_gained = random.randint(5, 10)
    health_gained = random.randint(1, 3)

    stamina_message = player.adjust_stamina(stamina_gained)
    health_message = player.adjust_health(health_gained)

    result = "You rest for a few minutes. <BR>{0}<BR>{1}".format(health_message, stamina_message)

    create_game_message(player, result)
    return redirect("game:play")


def attack(request):
    user = request.user
    player = get_object_or_None(Player, user=user)
    mob = get_object_or_None(Mob, player=player, active=True)

    if mob:
        outcome = attack_outcome(player, mob)
        damage_amount = 0

        player.adjust_stamina(ATTACK_STAMINA_COST)

        if outcome == AttackOutcome.miss or outcome == AttackOutcome.fumble:
            create_game_message(player, "Your attack misses.")

        elif outcome == AttackOutcome.critical:
            if player.weapon:
                damage_amount = calculate_damage(player, player.weapon.min_damage, player.weapon.max_damage, True)
            else:
                damage_amount = calculate_damage(player, 1, 5, True)

            create_game_message(player, "<span class='combat-critical'>Critical Strike!</span>")

        elif outcome == AttackOutcome.hit:
            if player.weapon:
                damage_amount = calculate_damage(player, player.weapon.min_damage, player.weapon.max_damage, True)
            else:
                damage_amount = calculate_damage(player, 1, 5, True)

        if damage_amount > 0:
            create_game_message(player, "You hit {0} for <span class='combat-damage'>{1}</span> damage!".format(
                mob.name, damage_amount))

            mob.adjust_health(damage_amount * -1)
            mob.save()

        if mob.current_health <= 0:
            result = "The {0} dies.".format(mob.name)
            create_game_message(player, result)

            result = combat_reward(player, mob)
            create_game_message(player, result)

            end_combat(player, mob)
        else:

            mob_outcome = attack_outcome(mob, player)
            mob.adjust_stamina(ATTACK_STAMINA_COST)
            mob.save()

            if mob_outcome == AttackOutcome.miss or mob_outcome == AttackOutcome.fumble:
                create_game_message(player, "The {0} tries to hit you but misses.".format(mob.name))

            elif mob_outcome == AttackOutcome.hit or mob_outcome == AttackOutcome.critical:
                damage_amount = calculate_damage(mob, 1, 6, False)
                create_game_message(
                    player, "The {0} hits you for <span class='combat-damage'>{1}</span> damage.".format(mob.name, damage_amount))

                player.adjust_health(damage_amount * -1)
                player.save()

    return redirect("game:play")


def run(request):
    user = request.user
    player = get_object_or_None(Player, user=user)
    mob = get_object_or_None(Mob, player=player, active=True)

    if player.current_stamina <= abs(RUN_STAMINA_COST):
        create_game_message(player, "You are too tired to run.")
    else:
        player.adjust_stamina(RUN_STAMINA_COST)

        if escape_combat(player, mob):
            # Success
            result = "You escape from battle."
            create_game_message(player, result)

            end_combat(player, mob)
        else:
            #Failure

            create_game_message(player, "You cannot escape.")

            mob_outcome = attack_outcome(mob, player)

            if mob_outcome == AttackOutcome.miss or mob_outcome == AttackOutcome.fumble:
                create_game_message(player, "The {0} tries to hit you but misses.".format(mob.name))

            elif mob_outcome == AttackOutcome.hit or mob_outcome == AttackOutcome.critical:
                damage_amount = calculate_damage(mob, 1, 6, False)
                create_game_message(
                    player, "The {0} hits you for <span class='combat-damage'>{1}</span> damage.".format(mob.name,
                                                                                                         damage_amount))

                player.adjust_health(damage_amount * -1)
                player.save()

        player.save()

    return redirect("game:play")
