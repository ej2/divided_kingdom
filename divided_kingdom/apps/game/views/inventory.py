from annoying.functions import get_object_or_None
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from divided_kingdom.apps.game.helpers.helper import create_game_message
from divided_kingdom.apps.item.helpers.armor import equip, remove
from divided_kingdom.apps.item.helpers.consumable import consume
from divided_kingdom.apps.item.helpers.weapon import wield
from divided_kingdom.apps.item.models import Item
from divided_kingdom.apps.player.decorators import player_required


@login_required
@player_required
def use_item(request, player, item_id):
    item = get_object_or_None(Item, pk=item_id)

    if item.base_type == "CON":
        result = consume(item, player)
        verb = item.action_verb or "use"
        message = "You {0} the {1}. {2}".format(verb, item.name, result)

    elif item.base_type == "WEA":
        wield(item, player)
        verb = item.action_verb or "use"
        message = "You {0} the {1}.".format(verb, item.name)

    elif item.base_type == "ARM":
        equip(item, player)
        verb = item.action_verb or "equip"
        message = "You {0} the {1}.".format(verb, item.name)

    elif item.base_type == "QST":
        message = "You don't know how to use the {0}.".format(item.name)

    create_game_message(player, message)

    return redirect("player:detail", player.pk)


@login_required
@player_required
def remove_equipment(request, player, item_id):
    item = get_object_or_None(Item, pk=item_id)
    remove(item, player)
    create_game_message(player, "You remove the {0}.".format(item.name))

    return redirect("player:detail", player.pk)


@login_required
@player_required
def drop_item(request, player, item_id):
    item = get_object_or_None(Item, pk=item_id)
    message = "You drop the {0}.".format(item.name)

    item.delete()
    create_game_message(player, message)

    return redirect("player:detail", player.pk)

